Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
})

var app = new Vue({
    el: '#app',
    data:{
        newItem:"",
        todos:[]
    },
    methods:{
        addItem:function(event){
            if(this.newItem == '')return;
            var todo = {
                id: this.newItem,
            };
            this.todos.push(todo);
            this.newItem = ''
        }
    }
})
